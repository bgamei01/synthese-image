#pragma once

#include <GL/glew.h>
#include "glm.hpp"

namespace glimac {

#define VERTEX_POSITION_LOCATION 0
#define VERTEX_NORMAL_LOCATION 1
#define VERTEX_TEX_COORD_LOCATION 2

struct ShapeVertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;

    static GLuint createVao(GLuint vbo, const GLuint *ibo = nullptr) {
        GLuint vao;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        if (ibo != nullptr) {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *ibo);
        }

        glEnableVertexAttribArray(VERTEX_POSITION_LOCATION);
        glEnableVertexAttribArray(VERTEX_NORMAL_LOCATION);
        glEnableVertexAttribArray(VERTEX_TEX_COORD_LOCATION);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(VERTEX_POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex),
                              (const GLvoid *) offsetof(ShapeVertex, position));
        glVertexAttribPointer(VERTEX_NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex),
                              (const GLvoid *) offsetof(ShapeVertex, normal));
        glVertexAttribPointer(VERTEX_TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex),
                              (const GLvoid *) offsetof(ShapeVertex, texCoords));
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);
        return vao;
    }
};

}
