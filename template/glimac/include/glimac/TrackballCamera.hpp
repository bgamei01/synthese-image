#pragma once

class TrackballCamera {

public:
    TrackballCamera() :
            m_fDistance{5},
            m_fAngleX{0},
            m_fAngleY{0} {}

    void moveFront(float delta) {
        m_fDistance += delta;
    }

    void rotateLeft(float degrees) {
        m_fAngleY += degrees;
    }

    void rotateUp(float degrees) {
        m_fAngleX += degrees;
    }

    glm::mat4 getViewMatrix() const {
        // return glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -m_fDistance));
        glm::mat4 M;
        M = glm::translate(M, glm::vec3(0, 0, -m_fDistance));
        M = glm::rotate(M, glm::radians(m_fAngleX), glm::vec3(1, 0, 0));
        M = glm::rotate(M, glm::radians(m_fAngleY), glm::vec3(0, 1, 0));
        return M;
    }

private:
    float m_fDistance;
    float m_fAngleX;
    float m_fAngleY;
};

struct TrackballController {

    explicit TrackballController() :
            motion{glm::vec3(0., 0., 0.)} {}

    virtual bool treatEvent(const SDL_Event &e) {
        if (e.type == SDL_QUIT) return true;

        if (e.type == SDL_MOUSEBUTTONUP) {
            if (e.motion.state == 5) {  // Down
                motion = {motion.x, motion.y, .5};
            }
            if (e.motion.state == 4) {  // Up
                motion = {motion.x, motion.y, -.5};
            }
        }
        if (e.type == SDL_MOUSEMOTION) {
            if (e.motion.state & SDL_BUTTON_LMASK) {
                motion = {e.motion.xrel, e.motion.yrel, motion.z};
            }
        }
        return false;
    }

    virtual void update(TrackballCamera& cam) {
        cam.moveFront(motion.z);
        cam.rotateLeft(motion.x);
        cam.rotateUp(motion.y);
        motion = {0., 0., 0.};
    }

    glm::vec3 motion;
};