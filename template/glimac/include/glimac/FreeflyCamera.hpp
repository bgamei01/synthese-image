#pragma once

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory>

class FreeflyCamera
{

public:
    FreeflyCamera() :
            m_fTheta{0},
            m_fPhi{glm::pi<float>()},
            m_Position{glm::vec3(0, 3, 10)}
    {
        computeDirectionVectors();
    }

    void moveLeft(float t)
    {
        m_Position += t * m_LeftVector;
        computeDirectionVectors();
    }

    void moveFront(float t)
    {
        m_Position += t * m_FrontVector;
        computeDirectionVectors();
    }

    void rotateLeft(float degrees)
    {
        m_fPhi += glm::radians(degrees);
        computeDirectionVectors();
    }

    void rotateUp(float degrees)
    {
        m_fTheta += glm::radians(degrees);
        m_fTheta = glm::clamp(m_fTheta, -glm::half_pi<float>(), glm::half_pi<float>());
        computeDirectionVectors();
    }

    void toggleYLock()
    {
        lockY = !lockY;
    }

    [[nodiscard]] glm::mat4 getViewMatrix() const
    {
        return glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
    }

    void reset()
    {
        lockY = true;
        m_fTheta = 0;
        m_fPhi = glm::pi<float>();
        m_Position = glm::vec3(0, 3, 10);
        computeDirectionVectors();
    }
    float m_fPhi;
    float m_fTheta;
private:

    void computeDirectionVectors()
    {
        m_FrontVector = {
                glm::cos(m_fTheta) * glm::sin(m_fPhi),
                glm::sin(m_fTheta),
                glm::cos(m_fTheta) * glm::cos(m_fPhi)
        };
        m_LeftVector = {glm::sin(m_fPhi + glm::half_pi<float>()), 0, glm::cos(m_fPhi + glm::half_pi<float>())};
        m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
        if (lockY)
        {
            m_Position.y = 3;
        }
    }

    glm::vec3 m_Position;
    glm::vec3 m_FrontVector;
    glm::vec3 m_LeftVector;
    glm::vec3 m_UpVector;
    bool lockY{true};
};

class FreeflyController
{

public:
    explicit FreeflyController() :
            motion{glm::vec4(0., 0., 0., 0.)}
    {
    }

    virtual bool treatEvent(const SDL_Event &e)
    {
        if (e.type == SDL_QUIT)
        {
            return true;
        }

        if (e.type == SDL_MOUSEMOTION)
        {
            if (e.motion.state & SDL_BUTTON_LMASK)
            {
                int x = reverseX ? -e.motion.xrel : e.motion.xrel;
                int y = reverseY ? -e.motion.yrel : e.motion.yrel;
                motion = {x / 2., y / 2., motion.z, motion.w};
            }
        }
        if (e.type == SDL_KEYDOWN)
        {
            auto sym = e.key.keysym.sym;
            auto shift = (e.key.keysym.mod & KMOD_SHIFT) != 0;
            auto ctrl = (e.key.keysym.mod & KMOD_CTRL) != 0;
            auto alt = (e.key.keysym.mod & KMOD_ALT) != 0;
            if (sym == SDLK_UP || sym == SDLK_z)
            {
                motion = {motion.x, motion.y, motion.z, .1};
            }
            else if (sym == SDLK_DOWN || sym == SDLK_s)
            {
                motion = {motion.x, motion.y, motion.z, -.1};
            }
            else if (sym == SDLK_LEFT || sym == SDLK_q)
            {
                motion = {motion.x, motion.y, .1, motion.w};
            }
            else if (sym == SDLK_RIGHT || sym == SDLK_d)
            {
                motion = {motion.x, motion.y, -.1, motion.w};
            }
            else if (sym == SDLK_m)
            {
                speed += (shift ? -1.f : 1.f) * (ctrl ? 1.f : .5f);
            }
            else if (sym == SDLK_x)
            {
                reverseX = !reverseX;
            }
            else if (sym == SDLK_y)
            {
                if (alt)
                {
                    toggleYLock = true;
                }
                else
                {
                    reverseY = !reverseY;
                }
            }
            else if (sym == SDLK_c)
            {
                reset = true;
            }
        }
        return false;
    }

    virtual void update(FreeflyCamera &cam)
    {
        motion *= speed;
        cam.rotateLeft(motion.x);
        cam.rotateUp(motion.y);
        cam.moveLeft(motion.z);
        cam.moveFront(motion.w);
        if (toggleYLock)
        {
            cam.toggleYLock();
        }
        if (reset)
        {
            reverseX = reverseY = reset = toggleYLock = false;
            speed = 2.0f;
            cam.reset();
        }
        motion = {0., 0., 0., 0.};
        toggleYLock = false;
    }

private:
    glm::vec4 motion;
    float speed = 2.0f;
    bool reverseX{false}, reverseY{false}, reset{false}, toggleYLock{false};
};