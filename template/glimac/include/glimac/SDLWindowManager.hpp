#pragma once

#include <cstdint>
#include <SDL/SDL.h>
#include "glm.hpp"

namespace glimac {

class SDLWindowManager {
public:
    SDLWindowManager(int width, int height, const char* title);

    ~SDLWindowManager();

    float ratio() const;

    bool pollEvent(SDL_Event& e);

    bool isKeyPressed(SDLKey key) const;

    // button can SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT and SDL_BUTTON_MIDDLE
    bool isMouseButtonPressed(uint32_t button) const;

    glm::ivec2 getMousePosition() const;

    void swapBuffers();

    // Return the time in seconds
    float getTime() const;

    int width, height;
};

}
