#pragma once

class DoublePendulum
{

public:

    void update(float t) {
        float t2 = t * 0.003f;
        float num1 = -g * (2 * m1 + m2) * sin(a1);
        float num2 = -m2 * g * sin(a1 - 2 * a2);
        float num3 = -2 * sin(a1 - a2) * m2;
        float num4 = a2_v * a2_v * r2 + a1_v * a1_v * r1 * cos(a1 - a2);
        float den = r1 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2));
        float a1_a = t2 * (num1 + num2 + num3 * num4) / den;

        num1 = 2 * sin(a1 - a2);
        num2 = (a1_v * a1_v * r1 * (m1 + m2));
        num3 = g * (m1 + m2) * cos(a1);
        num4 = a2_v * a2_v * r2 * m2 * cos(a1 - a2);
        den = r2 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2));
        float a2_a = t2 * (num1 * (num2 + num3 + num4)) / den;


        a1_v += a1_a;
        a2_v += a2_a;
        a1 += a1_v;
        a2 += a2_v;
        // Slows pendulum
        a1_v *= 0.995;
        a2_v *= 0.995;
    }



    void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM)
    {
        // Anchor
        _draw(program, PM, glm::scale(VM, glm::vec3(.4)), cube);
        // Tige 1
        glm::mat4 M = VM;
        M = glm::rotate(M, a1 - glm::half_pi<float>(), glm::vec3(0, 0, 1));
        M = glm::translate(M, glm::vec3(r1 / 2, 0, 0));
        _draw(program, PM, glm::rotate(glm::scale(M, glm::vec3(r1, .1, .1)), glm::half_pi<float>(), glm::vec3(0, 0, 1)), cylinder);
        // Middle point
        M = glm::translate(M, glm::vec3(r1 / 2, 0, 0));
        _draw(program, PM, glm::scale(M, glm::vec3(.2)), sphere);
        // Tige 2
        M = glm::rotate(M, a2, glm::vec3(0, 0, 1));
        M = glm::translate(M, glm::vec3(r2 / 2, 0, 0));
        _draw(program, PM, glm::rotate(glm::scale(M, glm::vec3(r2, .1, .1)), glm::half_pi<float>(), glm::vec3(0, 0, 1)), cylinder);
        // End point
        M = glm::translate(M, glm::vec3(r2 / 2, 0, 0));
        _draw(program, PM, glm::scale(M, glm::vec3(.2)), sphere);
    }

private:

    static void _draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM, const Canonical &model)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1f(program.uTexture, 0);
        glUniform1f(program.uShininess, 30);
        glUniform3fv(program.uDiffuse, 1, glm::value_ptr(glm::vec3(0.2)));
        glUniform3fv(program.uSpecular, 1, glm::value_ptr(glm::vec3(0.2)));
        glUniform3fv(program.uAmbient, 1, glm::value_ptr(glm::vec3(0.1)));
        glUniformMatrix4fv(program.uMVMatrix, 1, GL_FALSE, glm::value_ptr(VM));
        glUniformMatrix4fv(program.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * VM));
        glUniformMatrix4fv(program.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(VM))));
        model.draw();
    }

    float r1{1}, r2{1};
    float m1{2}, m2{2};
    float a1{glm::half_pi<float>()}, a2{glm::half_pi<float>()};
    float a1_v{0}, a2_v{0};
    float g{9.80};
    Sphere sphere{.5, 32, 16};
    Cylinder cylinder{1, 1, 20};
    Cube cube{};
};