#define GLM_SWIZZLE

#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/FreeflyCamera.hpp>
#include "glimac/FilePath.hpp"
#include "program/BaseProgram.hpp"
#include "canonical/Cylinder.hpp"
#include "Light.hpp"
#include "canonical/Cube.hpp"
#include "canonical/Sphere.hpp"
#include "bielle/Bielle.hpp"
#include "drawable/Barrel.hpp"
#include "drawable/Crate.hpp"
#include "pendulum/DoublePendulum.hpp"

class World
{
public:
    explicit World(float l, float d, float e) :
            bielle{l, d, e}
    {

    }

    void rotate(float t)
    {
        if (_pause)
        {
            return;
        }
        if (_is_bielle)
        {
            bielle.inc_angle(t * _factor);
        }
        else
        {
            pendulum.update(t * glm::clamp(_factor, 0.f, 180.f));
        }
    }

    bool treatEvent(const SDL_Event &e, std::vector<Light> &lights)
    {
        if (e.type == SDL_KEYDOWN)
        {
            auto sym = e.key.keysym.sym;
            bool shift = (e.key.keysym.mod & KMOD_SHIFT) != 0;
            bool ctrl = (e.key.keysym.mod & KMOD_CTRL) != 0;
            float factor = .02f * (ctrl ? 4.f : 1.f);

            if (sym == SDLK_ESCAPE)
            {
                return true;
            }
            if (sym == SDLK_l)
            {
                bielle.inc_l((shift ? -1.f : 1.f) * factor);
            }
            else if (sym == SDLK_k)
            {
                bielle.inc_d((shift ? -1.f : 1.f) * factor);
            }
            else if (sym == SDLK_j)
            {
                bielle.inc_e((shift ? -1.f : 1.f) * factor);
            }
            else if (sym == SDLK_SPACE)
            {
                _pause = !_pause;
            }
            else if (sym == SDLK_r)
            {
                _factor += (shift ? -1.f : 1.f) * (ctrl ? 5.f : 1.f);
                _factor = glm::clamp(_factor, -180.f, 180.f);
            }
            else if (sym == SDLK_KP0)
            {
                _draw_lights = !_draw_lights;
            }
            else if (sym >= SDLK_KP1 && sym <= SDLK_KP6)
            {
                int idx = sym - SDLK_KP1;
                lights[idx].toggle();
            }
            else if (sym == SDLK_t)
            {
                _is_bielle = !_is_bielle;
            }
        }
        return false;
    }

    void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM)
    {
        if (_is_bielle)
        {
            bielle.draw(program, PM, VM);
        }
        else
        {
            pendulum.draw(program, PM, VM);
        }
    }

    Bielle bielle;
    DoublePendulum pendulum;
    bool _pause = true;
    bool _draw_lights = true;
    bool _is_bielle = true;
    float _factor = 15;
};

void clearColor(const BaseProgram &program)
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUniform1i(program.uTexture, 0);
    glUniform1f(program.uShininess, 0);
    glUniform3fv(program.uDiffuse, 1, glm::value_ptr(glm::vec3(0)));
    glUniform3fv(program.uSpecular, 1, glm::value_ptr(glm::vec3(0)));
    glUniform3fv(program.uAmbient, 1, glm::value_ptr(glm::vec3(0)));
}

void loadTextures(
        const BaseProgram &program, const std::vector<Light> &lights,
        const glm::mat4 &VM
)
{
    glm::vec3 directions[6];
    glm::vec3 positions[6];
    glm::vec3 colors[6];
    int i = 0;
    for (const auto &light : lights)
    {
        if (!light.is_enabled())
        {
            continue;
        }
        directions[i] = light.get_direction(VM);
        positions[i] = light.get_position(VM);
        colors[i] = light.get_color();
        i += 1;
    }
    for (; i < lights.size(); i += 1)
    {
        colors[i] = glm::vec3(0);
    }

    glUniform3fv(program.uLightDirections, 6, (GLfloat *) (directions));
    glUniform3fv(program.uLightPositions, 6, (GLfloat *) (positions));
    glUniform3fv(program.uLightColors, 6, (GLfloat *) (colors));
}

void generateDecors(std::vector<Barrel> &barrels, std::vector<Crate> &crates, std::vector<Light> &lights)
{
    barrels.emplace_back(ShapeProperty(glm::vec3(30, -11, 22), glm::vec3(6, 8, 6), 0));
    barrels.emplace_back(ShapeProperty(glm::vec3(22, -11, 22), glm::vec3(6, 8, 6), 0));
    barrels.emplace_back(ShapeProperty(glm::vec3(14, -11, 22), glm::vec3(6, 8, 6), 0));
    barrels.emplace_back(ShapeProperty(glm::vec3(30, -11, -2), glm::vec3(6, 8, 6), 0));
    barrels.emplace_back(ShapeProperty(glm::vec3(37, -11, -2), glm::vec3(6, 8, 6), 0));
    barrels.emplace_back(ShapeProperty(glm::vec3(33.5, -3, -2), glm::vec3(6, 8, 6), 0));

    // Decors
    crates.emplace_back(ShapeProperty(glm::vec3(30, -5, -15), glm::vec3(20), 0), "crate1.png");
    crates.emplace_back(ShapeProperty(glm::vec3(14.5, -10, -20), glm::vec3(10), 0), "crate2.png");
    crates.emplace_back(ShapeProperty(glm::vec3(-12, -11.5, 21.5), glm::vec3(7), 0), "crate1.png");
    crates.emplace_back(ShapeProperty(glm::vec3(-37, -2.5, 0), glm::vec3(6, 25, 30), 0), "crate1.png");
    crates.emplace_back(ShapeProperty(glm::vec3(-20, -5, -21), glm::vec3(40, 20, 8), 0), "crate1.png");

    // Borders
    crates.emplace_back(ShapeProperty(glm::vec3(0, 0, 25 + .5), glm::vec3(80, 30, 1), 0), "border.png");
    crates.emplace_back(ShapeProperty(glm::vec3(0, 0, -25 - .5), glm::vec3(80, 30, 1), 0), "border.png");
    crates.emplace_back(ShapeProperty(glm::vec3(0, 15 + .5, 0), glm::vec3(80, 1, 50), 0), "border.png");
    crates.emplace_back(ShapeProperty(glm::vec3(0, -15 - .5, 0), glm::vec3(80, 1, 50), 0), "border.png");
    crates.emplace_back(ShapeProperty(glm::vec3(40 + .5, 0, 0), glm::vec3(1, 30, 50), 0), "border.png");
    crates.emplace_back(ShapeProperty(glm::vec3(-40 - .5, 0, 0), glm::vec3(1, 30, 50), 0), "border.png");

    float e = 0.1;
    // Cam light
    lights.emplace_back(2.f * glm::vec3(1), glm::vec3(0, 0, 0), .1, true, true);
    // Bielle light
    lights.emplace_back(glm::vec3(1), glm::vec3(0, 0, 0), .1, true);

    // Point lights
    lights.emplace_back(5.f * glm::vec3(1, .83, .5), glm::vec3(10, 13, -25 + e));
    lights.emplace_back(5.f * glm::vec3(1, .83, .5), glm::vec3(22, 13, 25 - e));
    lights.emplace_back(5.f * glm::vec3(1, .83, .5), glm::vec3(-12, 13, 25 - e));
    lights.emplace_back(5.f * glm::vec3(1, .83, .5), glm::vec3(-40 + e, 13, 20));
}

int main(int argc, char **argv)
{
    // Initialize SDL and open a window
    glimac::SDLWindowManager windowManager(400, 400, "template");
    SDL_EnableKeyRepeat(100, 10);

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if (GLEW_OK != glewInitError)
    {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }
    glimac::FilePath applicationPath(argv[0]);
    BaseProgram program(applicationPath);

    // Canonical Objects and Bielle
    Sphere sphere(.5, 32, 16);
    Cylinder cylinder(1, 1, 20);
    Cube cube;
    World world(2, 1, .17);

    // Decors
    std::vector<Barrel> barrels;
    std::vector<Crate> crates;
    std::vector<Light> lights;
    generateDecors(barrels, crates, lights);

    glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f), windowManager.ratio(), 0.1f, 100.f);

    FreeflyCamera cam;
    FreeflyController mH;

    glEnable(GL_DEPTH_TEST);
    // Application loop:
    bool done = false;
    float prev, next, t;
    SDL_Event ev;
    while (!done)
    {
        // Event loop:
        while (windowManager.pollEvent(ev))
        {
            done = done || mH.treatEvent(ev) || world.treatEvent(ev, lights);
        }
        mH.update(cam);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        program.program.use();
        clearColor(program);

        const glm::mat4 VM = cam.getViewMatrix();

        lights[1].set_pos(world.bielle.get_light_position());
        lights[1].set_angles(180 + world.bielle.get_light_angle(), 90);
        lights[0].set_angles(-glm::degrees(cam.m_fTheta) - 180, glm::degrees(cam.m_fPhi));

        loadTextures(program, lights, VM);
        if (world._draw_lights)
        {
            for (const auto &light : lights)
            {
                light.draw(program, ProjMatrix, VM, sphere);
            }
        }
        world.draw(program, ProjMatrix, VM);
        for (auto &barrel : barrels)
        {
            barrel.draw(program, ProjMatrix, VM);
        }
        for (const auto &crate : crates)
        {
            crate.draw(program, ProjMatrix, VM);
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // Update the display
        windowManager.swapBuffers();

        next = windowManager.getTime();
        world.rotate(next - prev);
        if (!world._pause)
        {
            t += next - prev;
        }
        prev = next;
    }

    return EXIT_SUCCESS;
}
