#pragma once

#include <string>
#include "glimac/Program.hpp"

struct BaseProgram
{
    glimac::Program program;
    GLuint programId;

    // Light
    GLint uLightPositions;
    GLint uLightDirections;
    GLint uLightColors;

    // Material
    GLint uTexture;
    GLint uShininess;
    GLint uAmbient;
    GLint uDiffuse;
    GLint uSpecular;

    // Matrix
    GLint uMVMatrix;
    GLint uMVPMatrix;
    GLint uNormalMatrix;

    explicit BaseProgram(const glimac::FilePath &applicationPath) :
            program(loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                                applicationPath.dirPath() + "shaders/3D.fs.glsl")),
            programId{program.getGLId()}
    {
        uTexture = glGetUniformLocation(programId, "uTexture");
        uLightColors = glGetUniformLocation(programId, "uLightColors");
        uLightPositions = glGetUniformLocation(programId, "uLightPositions");
        uLightDirections = glGetUniformLocation(programId, "uLightDirections");
        uShininess = glGetUniformLocation(programId, "uShininess");
        uAmbient = glGetUniformLocation(programId, "uAmbient");
        uDiffuse = glGetUniformLocation(programId, "uDiffuse");
        uSpecular = glGetUniformLocation(programId, "uSpecular");
        uMVMatrix = glGetUniformLocation(programId, "uMVMatrix");
        uMVPMatrix = glGetUniformLocation(programId, "uMVPMatrix");
        uNormalMatrix = glGetUniformLocation(programId, "uNormalMatrix");
    }
};