#pragma once

#include "../program/BaseProgram.hpp"
#include <glimac/glm.hpp>

class Drawable
{
public:
    virtual void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM) const = 0;
};