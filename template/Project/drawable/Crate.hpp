#pragma once

#include <glimac/FilePath.hpp>
#include <glimac/Image.hpp>

#include "Drawable.hpp"
#include "../canonical/Cube.hpp"
#include "../ShapeProperty.hpp"
#include "../Utils.hpp"

class Crate : public Drawable
{
public:
    Crate(ShapeProperty property, const std::string& texName = "crate1.png") :
            property{std::move(property)}
    {
        auto texture = loadImage(glimac::FilePath("../../assets/textures/decor/" + texName));
        generateTexture(&texId, texture);
    }

    void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM) const override
    {
        glm::mat4 M = VM;
        M = glm::translate(M, property.position);
        M = glm::scale(M, property.scale);

        glBindTexture(GL_TEXTURE_2D, texId);
        glUniform1f(program.uTexture, 0);
        glUniform1f(program.uShininess, property.shininess);
        glUniform3fv(program.uDiffuse, 1, glm::value_ptr(glm::vec3(0.2)));
        glUniform3fv(program.uSpecular, 1, glm::value_ptr(glm::vec3(0.2)));
        glUniform3fv(program.uAmbient, 1, glm::value_ptr(glm::vec3(0.1)));
        glUniformMatrix4fv(program.uMVMatrix, 1, GL_FALSE, glm::value_ptr(M));
        glUniformMatrix4fv(program.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * M));
        glUniformMatrix4fv(program.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(M))));
        cube.draw();
    }

private:
    GLuint texId;
    Cube cube;
    ShapeProperty property;
};