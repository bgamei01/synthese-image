#pragma once

#include <iostream>
#include <glimac/glm.hpp>

#include "BielleRail.hpp"
#include "BielleArm.hpp"
#include "BielleTree.hpp"
#include "BiellePiston.hpp"
#include "../canonical/Canonical.hpp"


class Bielle
{
public:
    Bielle(float l, float d, float e) :
            rail{BielleRail(&(this->l), &(this->d), &(this->e))},
            arm{BielleArm(&(this->d), &(this->e))},
            tree{BielleTree(&(this->l), &(this->e))},
            piston{BiellePiston(&(this->e))},
            l{l},
            d{d},
            e{e}
    {
        recompute_all();
    }

    void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM)
    {
        float a = glm::radians(alpha);
        float h = d * glm::sin(a);
        float gamma = glm::acos(h / l);
        float beta = glm::half_pi<float>() + a - gamma;

        float dx = l * glm::sin(gamma) / 2.0f + d * glm::cos(a);
        float dy = l * glm::cos(gamma) / 2.0f;

        rail.draw(program, PM, glm::translate(VM, glm::vec3(0, 0, -e / 2)));
        glm::mat4 MVM = VM;
        MVM = glm::rotate(MVM, a, glm::vec3(0, 0, 1));
        arm.draw(program, PM, MVM);
        MVM = glm::translate(MVM, glm::vec3(d, 0, 2 * e));
        MVM = glm::rotate(MVM, -beta, glm::vec3(0, 0, 1));
        tree.draw(program, PM, MVM);
        MVM = glm::translate(MVM, glm::vec3(l, 0, -2 * e));
        MVM = glm::rotate(MVM, glm::half_pi<float>() - gamma, glm::vec3(0, 0, 1));
        piston.draw(program, PM, MVM);

        lightPosition = glm::vec3(dx, dy + e / 2, 2 * e);
        lightAngle = 90 - glm::degrees(gamma);
    }

    void inc_angle(float t)
    {
        alpha += t;
    }

    void inc_l(float delta)
    {
        l += delta;
        recompute_all();
    }

    void inc_d(float delta)
    {
        d += delta;
        recompute_all();
    }

    void inc_e(float delta)
    {
        e += delta;
        recompute_all();
    }

    [[nodiscard]] glm::vec3 get_light_position() const
    {
        return lightPosition;
    }

    [[nodiscard]] float get_light_angle() const
    {
        return lightAngle;
    }

private:
    void recompute_all()
    {
        l = glm::clamp(l, 1.f, 10.f);
        d = glm::clamp(d, .5f, 5 * l / 11);
        e = glm::clamp(e, .05f, d / 4);
    }

    float alpha = 60;
    float l, d, e;
    BielleRail rail;
    BielleArm arm;
    BielleTree tree;
    BiellePiston piston;
    glm::vec3 lightPosition;
    float lightAngle;
};