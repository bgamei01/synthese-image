#pragma once

#include <glimac/FilePath.hpp>
#include <glimac/Image.hpp>

#include "../ShapeProperty.hpp"
#include "../Utils.hpp"
#include "../canonical/Canonical.hpp"


class BielleArm
{
public:
    BielleArm(float *d, float *e) :
            d{d},
            e{e}
    {
        generateTextures();
    }

    void draw(const BaseProgram &prog, const glm::mat4 &PM, const glm::mat4 &VM) const
    {
        glm::vec3 color(.2);
        glUniform1f(prog.uShininess, 32);
        glUniform3fv(prog.uDiffuse, 1, glm::value_ptr(color));
        glUniform3fv(prog.uSpecular, 1, glm::value_ptr(glm::vec3(1.0, 0.97, 0.86)));
        glUniform3fv(prog.uAmbient, 1, glm::value_ptr(color));

        float d = *(this->d);
        float e = *(this->e);
        float d2 = d / 2;
        float d4 = d / 4;
        float e2 = e / 2;
        float ee = e * 2;
        auto pi2 = glm::half_pi<float>();

        glUniform1i(prog.uTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, b1);
        draw(prog, PM, VM, cube, glm::vec3(d4, 0, e2), glm::vec3(d + d2, ee, e));
        glBindTexture(GL_TEXTURE_2D, b2);
        draw(prog, PM, VM, cube, glm::vec3(-d + d4, 0, e), glm::vec3(d2, d2, ee));
        glBindTexture(GL_TEXTURE_2D, c1);
        draw(prog, PM, VM, cylinder, glm::vec3(0, 0, e + e2), glm::vec3(e), pi2);
        glBindTexture(GL_TEXTURE_2D, c2);
        draw(prog, PM, VM, cylinder, glm::vec3(d, 0, e + e2), glm::vec3(e), pi2);
        glBindTexture(GL_TEXTURE_2D, hc1);
        draw(prog, PM, VM, halfCylinder, glm::vec3(d, 0, e2), glm::vec3(ee, e, ee), pi2, pi2);
    }

private:
    void generateTextures() {
        auto b1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/arm/b1.png"));
        auto b2Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/arm/b2.png"));
        auto c1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/arm/c1.png"));
        auto c2Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/arm/c2.png"));
        auto hc1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/arm/hc1.png"));

        generateTexture(&b1, b1Tex);
        generateTexture(&b2, b2Tex);
        generateTexture(&c1, c1Tex);
        generateTexture(&c2, c2Tex);
        generateTexture(&hc1, hc1Tex);
    }
    static void draw(
            const BaseProgram &prog,
            const glm::mat4 &PM,
            const glm::mat4 &VM,
            const Canonical &d,
            const glm::vec3 &position,
            const glm::vec3 &scale,
            float angleX = 0,
            float angleY = 0
    )
    {
        glm::mat4 MVM = VM;
        MVM = glm::translate(MVM, position);
        MVM = glm::rotate(MVM, angleX, glm::vec3(1, 0, 0));
        MVM = glm::rotate(MVM, angleY, glm::vec3(0, 1, 0));
        MVM = glm::scale(MVM, scale);
        glUniformMatrix4fv(prog.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVM));
        glUniformMatrix4fv(prog.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * MVM));
        glUniformMatrix4fv(prog.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(MVM))));

        d.draw();
    }

    float *d, *e;
    GLuint b1, b2, c1, c2, hc1;
    Cylinder cylinder{1, 1, 20};
    Cylinder halfCylinder{1, 1, 10, glm::pi<float>()};
    Cube cube{};
};