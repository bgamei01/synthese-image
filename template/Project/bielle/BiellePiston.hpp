#pragma once

#include <glimac/FilePath.hpp>
#include <glimac/Image.hpp>

#include "../ShapeProperty.hpp"
#include "../Utils.hpp"
#include "../canonical/Canonical.hpp"


class BiellePiston
{
public:
    explicit BiellePiston(float *e) : e{e}
    {
        generateTextures();
    }

    void draw(const BaseProgram &prog, const glm::mat4 &PM, const glm::mat4 &VM) const
    {
        glm::vec3 color(.2);
        glUniform1f(prog.uShininess, 32);
        glUniform3fv(prog.uDiffuse, 1, glm::value_ptr(color));
        glUniform3fv(prog.uSpecular, 1, glm::value_ptr(glm::vec3(1.0, 0.97, 0.86)));
        glUniform3fv(prog.uAmbient, 1, glm::value_ptr(color));

        float e = *(this->e);
        float e2 = e / 2;
        float e4 = e / 4;
        float e8 = e / 8;
        float ee = e * 2;
        float eee = e * 3;

        glUniform1i(prog.uTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, b1);
        draw(prog, PM, VM, cube, glm::vec3(e2, 0, e2), glm::vec3(ee, ee, e));
        glBindTexture(GL_TEXTURE_2D, b2);
        draw(prog, PM, VM, cube, glm::vec3(e2, e + e8, e2), glm::vec3(ee, e4, eee));
        glBindTexture(GL_TEXTURE_2D, b3);
        draw(prog, PM, VM, cube, glm::vec3(e2, -e - e8, e2), glm::vec3(ee, e4, eee));
    }

private:

    void generateTextures() {
        auto b1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/piston/b1.png"));
        auto b2Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/piston/b2.png"));
        auto b3Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/piston/b3.png"));

        generateTexture(&b1, b1Tex);
        generateTexture(&b2, b2Tex);
        generateTexture(&b3, b3Tex);
    }

    static void draw(
            const BaseProgram &prog,
            const glm::mat4 &PM,
            const glm::mat4 &VM,
            const Canonical &d,
            const glm::vec3 &position,
            const glm::vec3 &scale
    )
    {
        glm::mat4 MVM = VM;
        MVM = glm::translate(MVM, position);
        MVM = glm::scale(MVM, scale);
        glUniformMatrix4fv(prog.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVM));
        glUniformMatrix4fv(prog.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * MVM));
        glUniformMatrix4fv(prog.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(MVM))));

        d.draw();
    }

    GLuint b1, b2, b3;
    float *e;
    Cube cube{};
};