#pragma once

#include <glimac/FilePath.hpp>
#include <glimac/Image.hpp>

#include "../ShapeProperty.hpp"
#include "../Utils.hpp"
#include "../canonical/Canonical.hpp"

class BielleRail
{
public:
    BielleRail(float *l, float *d, float *e) :
            l{l},
            d{d},
            e{e}
    {
        generateTextures();
    }

    void draw(const BaseProgram &prog, const glm::mat4 &PM, const glm::mat4 &VM) const
    {
        glm::vec3 color(.15);
        glUniform1f(prog.uShininess, 32);
        glUniform3fv(prog.uDiffuse, 1, glm::value_ptr(color));
        glUniform3fv(prog.uSpecular, 1, glm::value_ptr(glm::vec3(0.1)));
        glUniform3fv(prog.uAmbient, 1, glm::value_ptr(color));

        float e = *(this->e);
        float ee = e * 2;
        auto pi2 = glm::half_pi<float>();
        float lde = *l + *d + ee;

        glUniform1i(prog.uTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, b1);
        draw(prog, PM, VM, cube, glm::vec3(lde / 2, 0, 0), glm::vec3(lde, ee, e));
        glBindTexture(GL_TEXTURE_2D, hc1);
        draw(prog, PM, VM, halfCylinder, glm::vec3(0), glm::vec3(ee, e, ee), pi2, -pi2);
        glBindTexture(GL_TEXTURE_2D, hc2);
        draw(prog, PM, VM, halfCylinder, glm::vec3(lde, 0, 0), glm::vec3(ee, e, ee), pi2, pi2);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

private:

    void generateTextures() {
        auto b1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/rail/b1.png"));
        auto hc1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/rail/hc1.png"));
        auto hc2Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/rail/hc2.png"));

        generateTexture(&b1, b1Tex);
        generateTexture(&hc1, hc1Tex);
        generateTexture(&hc2, hc2Tex);
    }

    static void draw(
            const BaseProgram &prog,
            const glm::mat4 &PM,
            const glm::mat4 &VM,
            const Canonical &d,
            const glm::vec3 &position,
            const glm::vec3 &scale,
            float angleX = 0,
            float angleY = 0
    )
    {
        glm::mat4 MVM = VM;
        MVM = glm::translate(MVM, position);
        MVM = glm::rotate(MVM, angleX, glm::vec3(1, 0, 0));
        MVM = glm::rotate(MVM, angleY, glm::vec3(0, 1, 0));
        MVM = glm::scale(MVM, scale);
        glUniformMatrix4fv(prog.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVM));
        glUniformMatrix4fv(prog.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * MVM));
        glUniformMatrix4fv(prog.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(MVM))));

        d.draw();
    }

    float *l, *d, *e;
    GLuint b1, hc1, hc2;
    Cube cube{};
    Cylinder halfCylinder{1, 1, 10, glm::pi<float>()};
};