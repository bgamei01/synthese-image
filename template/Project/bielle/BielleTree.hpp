#pragma once

#include <glimac/FilePath.hpp>
#include <glimac/Image.hpp>

#include "../ShapeProperty.hpp"
#include "../Utils.hpp"
#include "../canonical/Canonical.hpp"

class BielleTree
{
public:
    BielleTree(float *l, float *e) :
            l{l},
            e{e}
    {
        generateTextures();
    }

    void draw(const BaseProgram &prog, const glm::mat4 &PM, const glm::mat4 &VM) const
    {
        glm::vec3 color(.2);
        glUniform1f(prog.uShininess, 32);
        glUniform3fv(prog.uDiffuse, 1, glm::value_ptr(color));
        glUniform3fv(prog.uSpecular, 1, glm::value_ptr(glm::vec3(1.0, 0.97, 0.86)));
        glUniform3fv(prog.uAmbient, 1, glm::value_ptr(color));

        float l = *(this->l);
        float e = *(this->e);
        float e2 = e / 2;
        float l2 = l / 2;
        auto pi2 = glm::half_pi<float>();

        glUniform1i(prog.uTexture, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, b1);
        draw(prog, PM, VM, cube, glm::vec3(0), glm::vec3(e, e, e));
        glBindTexture(GL_TEXTURE_2D, b2);
        draw(prog, PM, VM, cube, glm::vec3(l, 0, 0), glm::vec3(e, e, e));
        glBindTexture(GL_TEXTURE_2D, c1);
        draw(prog, PM, VM, cylinder, glm::vec3(l2, 0, 0), glm::vec3(e2, l-e, e2), pi2, pi2);
    }

private:

    void generateTextures() {
        auto b1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/tree/b1.png"));
        auto b2Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/tree/b2.png"));
        auto c1Tex = loadImage(glimac::FilePath("../../assets/textures/bielle/tree/c1.png"));

        generateTexture(&b1, b1Tex);
        generateTexture(&b2, b2Tex);
        generateTexture(&c1, c1Tex);
    }

    static void draw(
            const BaseProgram &prog,
            const glm::mat4 &PM,
            const glm::mat4 &VM,
            const Canonical &d,
            const glm::vec3 &position,
            const glm::vec3 &scale,
            float angleX = 0,
            float angleY = 0
    )
    {
        glm::mat4 MVM = VM;
        MVM = glm::translate(MVM, position);
        MVM = glm::rotate(MVM, angleY, glm::vec3(0, 1, 0));
        MVM = glm::rotate(MVM, angleX, glm::vec3(1, 0, 0));
        MVM = glm::scale(MVM, scale);
        glUniformMatrix4fv(prog.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVM));
        glUniformMatrix4fv(prog.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * MVM));
        glUniformMatrix4fv(prog.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(MVM))));

        d.draw();
    }

    float *l, *e;
    GLuint b1, b2, c1;
    Cylinder cylinder{1, 1, 20};
    Cube cube{};
};