#pragma once

#include <vector>
#include "glimac/common.hpp"
#include "../program/BaseProgram.hpp"
#include "../ShapeProperty.hpp"
#include "Canonical.hpp"

class Cylinder : public Canonical
{
    typedef glimac::ShapeVertex Vertex;
public:
    Cylinder(float height, float width, int resolution, float maxAngle = 2 * glm::pi<float>()) :
            resolution{resolution},
            width{width},
            height{height},
            maxAngle{maxAngle},
            vertices{
                    std::vector<Vertex>(4 * (resolution + 1) + 2)
            } // circles size = 2 * resolution + 4; border size = 2 * resolution + 2
    {
        constructCylinder();
        constructVboVao();
    }

    void draw() const override
    {
        int r = resolution + 2;
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_FAN, 0, r);    // Upper circle
        glDrawArrays(GL_TRIANGLE_FAN, r, r);    // Lower circle
        glDrawArrays(GL_TRIANGLE_STRIP, 2 * r, 2 * (r - 1)); // Border
        glBindVertexArray(0);
    }

private:
    void constructCylinder()
    {
        const float theta = maxAngle / (float) resolution;
        const int r = resolution + 2;
        const float h2 = height / 2;
        const float w2 = width / 2;

        glm::vec3 upNormal{0, 1, 0};
        glm::vec3 downNormal{0, -1, 0};
        glm::vec2 topTexCenter{.25, .75};
        glm::vec2 bottomTexCenter{.75, .75};

        // Circle centers
        vertices[0] = {glm::vec3(0, .5, 0), upNormal, topTexCenter};
        vertices[r] = {glm::vec3(0, -.5, 0), downNormal, bottomTexCenter};

        for (int i = 0; i < r - 1; i += 1)
        {
            const double x = 1.0 * i / resolution;
            const double angle = 1.0 * theta * i;
            const double c = cos(angle);
            const double s = sin(angle);

            const glm::vec3 bottomPos{w2 * c, -h2, w2 * s};
            const glm::vec3 topPos{w2 * c, h2, w2 * s};
            const glm::vec3 sideNormal{c, 0, s};
            const glm::vec2 texPos{c / 4, s / 4};

            // Circles and borders
            vertices[1 + i] = {topPos, upNormal, topTexCenter + texPos};
            vertices[1 + r + i] = {bottomPos, downNormal, bottomTexCenter + texPos};
            vertices[2 * r + 2 * i] = {topPos, sideNormal, glm::vec2(x, 0.0)};
            vertices[2 * r + 2 * i + 1] = {bottomPos, sideNormal, glm::vec2(x, 0.5)};
        }
    }

    void constructVboVao()
    {
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        vao = Vertex::createVao(vbo);
    }

    std::vector<Vertex> vertices;
    GLuint vao{}, vbo{};
    float width, height;
    float maxAngle;
    int resolution;
};