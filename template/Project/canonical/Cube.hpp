#pragma once

#include "Canonical.hpp"
#include "glimac/common.hpp"
#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include <vector>
#include <iostream>

class Cube : public Canonical
{
    typedef glimac::ShapeVertex Vertex;
public:
    Cube()
    {
        constructCube();
        constructVboVao();
    }

    void draw() const override
    {
        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

private:
    void constructCube()
    {
        float x = .5;
        vertices = {
                {{x,  x,  x},  {0,  1,  0},  {1. / 3., 0.0}},
                {{x,  x,  -x}, {0,  1,  0},  {1. / 3., 1. / 2.}},
                {{-x, x,  -x}, {0,  1,  0},  {0.0,     1. / 2.}},
                {{-x, x,  x},  {0,  1,  0},  {0.0,     0.0}},

                {{x,  x,  x},  {1,  0,  0},  {2. / 3., 0.0}},
                {{x,  -x, x},  {1,  0,  0},  {2. / 3., 1. / 2.}},
                {{x,  -x, -x}, {1,  0,  0},  {1. / 3., 1. / 2.}},
                {{x,  x,  -x}, {1,  0,  0},  {1. / 3., 0.0}},

                {{x,  x,  x},  {0,  0,  1},  {1.0,     0.0}},
                {{x,  -x, x},  {0,  0,  1},  {1.0,     1. / 2.}},
                {{-x, -x, x},  {0,  0,  1},  {2. / 3., 1. / 2.}},
                {{-x, x,  x},  {0,  0,  1},  {2. / 3., 0.0}},

                {{-x, -x, -x}, {0,  -1, 0},  {0.0,     1.0}},
                {{-x, -x, x},  {0,  -1, 0},  {0.0,     1. / 2.}},
                {{x,  -x, x},  {0,  -1, 0},  {1. / 3., 1. / 2.}},
                {{x,  -x, -x}, {0,  -1, 0},  {1. / 3., 1.0}},

                {{-x, -x, -x}, {-1, 0,  0},  {1. / 3., 1.0}},
                {{-x, x,  -x}, {-1, 0,  0},  {1. / 3., 1. / 2.}},
                {{-x, x,  x},  {-1, 0,  0},  {2. / 3., 1. / 2.}},
                {{-x, -x, x},  {-1, 0,  0},  {2. / 3., 1.0}},

                {{-x, -x, -x}, {0,  0,  -1}, {2. / 3., 1.0}},
                {{-x, x,  -x}, {0,  0,  -1}, {2. / 3., 1. / 2.}},
                {{x,  x,  -x}, {0,  0,  -1}, {1.0,     1. / 2.}},
                {{x,  -x, -x}, {0,  0,  -1}, {1.0,     1.0}}
        };
        for (uint32_t i = 0; i < vertices.size(); i += 4)
        {
            indices.insert(indices.end(), {i, i + 1, i + 2, i, i + 2, i + 3});
        }
    }

    void constructVboVao()
    {
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1, &ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), &indices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        vao = Vertex::createVao(vbo, &ibo);
    }

    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
    GLuint vao{}, vbo{}, ibo{};
};