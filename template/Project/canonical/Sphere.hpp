#pragma once

#include <vector>

#include "glimac/common.hpp"
#include "Canonical.hpp"

class Sphere: public Canonical {
    typedef glimac::ShapeVertex Vertex;
public:
    Sphere(GLfloat radius, GLsizei discLat, GLsizei discLong) {
        build(radius, discLat, discLong);
        constructVboVao();
    }

    void draw() const override {
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());
        glBindVertexArray(0);
    }

private:
    void constructVboVao() {
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        vao = Vertex::createVao(vbo);
    }

    void build(GLfloat r, GLsizei discLat, GLsizei discLong) {
        GLfloat rcpLat = 1.f / discLat, rcpLong = 1.f / discLong;
        GLfloat dPhi = 2 * glm::pi<float>() * rcpLat, dTheta = glm::pi<float>() * rcpLong;

        std::vector<Vertex> data;

        for(GLsizei j = 0; j <= discLong; ++j) {
            GLfloat cosTheta = glm::cos(-glm::pi<float>() / 2 + j * dTheta);
            GLfloat sinTheta = glm::sin(-glm::pi<float>() / 2 + j * dTheta);

            for(GLsizei i = 0; i <= discLat; ++i) {
                Vertex vertex;

                vertex.texCoords.x = i * rcpLat;
                vertex.texCoords.y = 1.f - j * rcpLong;

                vertex.normal.x = glm::sin(i * dPhi) * cosTheta;
                vertex.normal.y = sinTheta;
                vertex.normal.z = glm::cos(i * dPhi) * cosTheta;

                vertex.position = r * vertex.normal;

                data.push_back(vertex);
            }
        }

        for(GLsizei j = 0; j < discLong; ++j) {
            GLsizei offset = j * (discLat + 1);
            for(GLsizei i = 0; i < discLat; ++i) {
                vertices.push_back(data[offset + i]);
                vertices.push_back(data[offset + (i + 1)]);
                vertices.push_back(data[offset + discLat + 1 + (i + 1)]);
                vertices.push_back(data[offset + i]);
                vertices.push_back(data[offset + discLat + 1 + (i + 1)]);
                vertices.push_back(data[offset + i + discLat + 1]);
            }
        }
    }

    std::vector<Vertex> vertices;
    GLuint vao{}, vbo{};
};