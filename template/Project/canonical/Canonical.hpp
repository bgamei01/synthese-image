#pragma once

class Canonical
{
public:
    virtual void draw() const = 0;
};