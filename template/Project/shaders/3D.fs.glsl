#version 300 es

precision mediump float;

// Material
uniform vec3 uAmbient;
uniform vec3 uDiffuse;
uniform vec3 uSpecular;
uniform float uShininess;
uniform sampler2D uTexture;

// Light
uniform vec3 uLightColors[6];
uniform vec3 uLightPositions[6];
uniform vec3 uLightDirections[6];

in vec3 vPosition;
in vec3 vNormal;
in vec2 vUV;

out vec3 fFragColor;

const float zero = 0.00001;

vec3 blinnPhong(vec3 lightPosition, vec3 lightDirection, vec3 lightColor) {
    vec3 ld = lightDirection;
    vec3 lp = lightPosition;
    vec3 li = lightColor;

    vec3 wi = normalize(lp - vPosition);
    vec3 wo = normalize(-vPosition);
    vec3 N = normalize(vNormal);
    float d = distance(vPosition, lp);
    vec3 lightIntensity = li / d;

    // If this is a spot light but the angle is to big -> no light
    if (length(lightDirection) >= zero && dot(wi, ld) <= .8) {
        return vec3(0);
    }

    vec3 halfVec = normalize((wo + wi) / 2.0);

    float diffuse = clamp(dot(wi, N), 0.0, 1.0);
    float angle2 = clamp(dot(halfVec, N), 0.0, 1.0);
    float specular = pow(diffuse < zero ? zero : angle2, uShininess);

    return lightIntensity * (uDiffuse * diffuse + uSpecular * specular);
}

vec3 blinnPhong() {
    vec3 color = vec3(0);
    for (int i = 0; i < 6; i++) {
        if (length(uLightColors[i]) < zero) break;
        color += blinnPhong(uLightPositions[i], uLightDirections[i], uLightColors[i]);
    }
    return color;
}

void main() {
    vec3 texColor = texture(uTexture, vUV).xyz;
    if (length(texColor) < zero) {
        fFragColor = uAmbient + blinnPhong();
    } else {
        fFragColor = texColor * uAmbient + blinnPhong();
    }
}
