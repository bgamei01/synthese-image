#version 300 es

layout(location = 0) in vec3 attPosition;
layout(location = 1) in vec3 attNormal;
layout(location = 2) in vec2 attUV;

uniform mat4 uMVMatrix;
uniform mat4 uMVPMatrix;
uniform mat4 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vUV;

void main() {
  vUV = attUV;

  vec4 position = vec4(attPosition, 1);
  vec4 normal = vec4(attNormal, 0);

  vPosition = vec3(uMVMatrix * position);
  vNormal = vec3(uNormalMatrix * normal);

  gl_Position = uMVPMatrix * position;
}
