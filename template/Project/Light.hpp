#pragma once

#include <utility>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include "canonical/Sphere.hpp"

class Light
{
public:
    Light(
            const glm::vec3 &color,
            const glm::vec3 &position,
            float scale = .5f,
            bool isSpot = false,
            bool camLight = false
    ) :
            color(color),
            position(position),
            scale(scale),
            isSpot(isSpot),
            camLight(camLight)
    {
    }

    [[nodiscard]] bool is_enabled() const
    {
        return enabled;
    }

    [[nodiscard]] glm::vec3 get_position(const glm::mat4& VM) const
    {
        if (camLight) {
            return glm::vec3(0);
        }
        return (VM * glm::vec4(position, 1)).xyz();
    }

    [[nodiscard]] glm::vec3 get_color() const
    {
        return color;
    }

    [[nodiscard]] glm::vec3 get_direction(const glm::mat4& VM) const
    {
        if (!isSpot) {
            return glm::vec3(0);
        }
        glm::mat4 M;
        M = glm::rotate(M, glm::radians(angleY), glm::vec3(0, 1, 0));
        M = glm::rotate(M, glm::radians(angleX), glm::vec3(1, 0, 0));
        return (VM * M * glm::vec4(0, 0, 1, 0)).xyz();
    }

    void toggle()
    {
        enabled = !enabled;
    }

    void set_pos(const glm::vec3 &pos)
    {
        position = pos;
    }

    void set_angles(float _angleX, float _angleY)
    {
        angleX = _angleX;
        angleY = _angleY;
    }

    void draw(const BaseProgram &program, const glm::mat4 &PM, const glm::mat4 &VM, const Canonical &model) const
    {
        if (camLight)
        {
            return;
        }
        glm::mat4 LMM = glm::scale(glm::translate(VM, position), glm::vec3(scale));
        glUniform3fv(program.uAmbient, 1, glm::value_ptr(enabled ? color : glm::vec3(.2)));
        glUniformMatrix4fv(program.uMVMatrix, 1, GL_FALSE, glm::value_ptr(LMM));
        glUniformMatrix4fv(program.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(PM * LMM));
        glUniformMatrix4fv(program.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(LMM))));
        model.draw();
    }

    bool isSpot = false;

private:
    float scale;
    glm::vec3 color;
    glm::vec3 position;
    float angleY{0}, angleX{0};
    bool enabled = true;
    bool camLight = false;
};