#pragma once

#include <glm/vec3.hpp>
#include <GL/glew.h>

struct ShapeProperty {
    ShapeProperty(const glm::vec3 &position, const glm::vec3 &scale, float shininess)
            : position(position), scale(scale), shininess(shininess) {}

    glm::vec3 position;
    glm::vec3 scale;
    float shininess;
};