# Synthese Image

Project of image synthesis.

## Compilation

To compile this project open a terminal in the template folder and run the following commands:
```sh
mkdir -p build
cd build
cmake ..
make
```

And then run the project with (using the same terminal) :
```sh
cd Project
./Project_main
```

The simulation take some time to start because some textures are big.

## Controls

### Indications

`-` indicate that if shift is pressed with the key then the action is inverted.  
eg. `r` increases Bielle's rotation speed and `Shift + r` decrease rotation speed.

`*` indicate that if ctrl is pressed with the key then the action is powerful.  
eg. `r` increase Bielle's rotation speed by x and `Ctrl + r` increase rotation speed by 4x
(not always 4 time bigger).

> Obviously a key with both indicator allow: key, shift + key, ctrl + key and shift + ctrl + key.

`|` indicate that either one of the two key can be pressed.

### Keymap

Global controls:
- `Esc` : quit program
- `Space` : stop/start rotation
- `t` : switch between Bielle and Double Pendulum

Camera controls:
- `z`|`Up_Arrow` : move forward
- `s`|`Down_Arrow` : move backward
- `q`|`Left_Arrow` : move left
- `d`|`Right_Arrow` : move right
- `x` : invert mouse x-coordinate (invert left and right)
- `y` : invert mouse y-coordinate (invert up and down)
- `-*m` : increase movement speed
- `c` : reset camera position, rotation and speed
- `alt + y` : lock/unlock y position;

Bielle modifiers:
- `-*l` : change tree length (`l` parameter)
- `-*k` : change arm length (`d` parameter)
- `-*j` : change piston length (`e` parameter)
- `-*r` : change rotation speed

Light switch: (only numpad)
- `0` : hide/show light bulbs
- `1` : turn on/off camera light
- `2` : turn on/off bielle light
- `3` : turn on/off light above stacked crates
- `4` : turn on/off light above barrel
- `5` : turn on/off light above alone crate
- `6` : turn on/off light next  to shelf

### Notes

Decreasing tree/arm length can change other parameters to ensure coherence.

## Upgrade done

Modification of parameters (l, d, e) in-game.
Other mechanical model : Double pendulum.

## Unimplemented features

Cannot enable fullscreen. Every call to `SDL_WM_ToggleFullScreen`
results in me restarting my computer.  
Even adding flag `SDL_FULLSCREEN` to the existing `SDL_SetVideoMode` call result in
a black screen.

Note 04/01/2022 : Fullscreen working on uni's computers.  
Note 04/01/2022 : Fake fullscreen working on my computer.

## Expected result

![Image](preview.png)
